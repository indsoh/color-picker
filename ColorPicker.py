###############################################################################
# This is a simple color picker program. Instead of a slider to alter the value
# of brightness/darkness the mouse is held down instead.
###############################################################################

from pyprocessing import *
import colorsys

def setup() : 
    global font
    global value
    global clicks
    global delta

    font    = createFont("Arial", 10, True)
    value   = 1
    clicks  = 0
    delta   = .01

    # There are some magic numbers but creating variables for them didn't add
    # any readability. For reference, 720 is the width of the window and 175
    # is the height. The color codes are produced from using the coordinates.
    # The dimensions of the bottom display space is 720 pixels wide and 25
    # pixels high. This is why 150 is used for calculating colors and not 175.
    size(720, 175)
    background(255, 255, 255)
    colorMode(HSB, 1, 1, 1)

    # Color the rainbow
    for hue in range(0, 720, 1) :
        for sat in range(0, 150, 1) :
            stroke(hue / 720.0, sat / 150.0, 1)
            point(hue, 150 - sat)

def hsv2rgb(h,s,v) :
    return tuple(int(i * 255) for i in colorsys.hsv_to_rgb(h,s,v))

# When the mouse is clicked and held, the value goes down in steps of .01 and
# once 0 (or slightly under) then the value goes up in steps of .01 on and on as
# long as the mouse is held. This gives a pulsing appearance as the display
# alterates between going darker and brighter.
#
# If the mouse is released, the pulsing haults at the current darkness or
# brightness. If the mouse is clicked again the value resets back to 1 which is
# the default and soft max value.
#
# An odd number of mouse clicks corresponds in changing the value while an even
# number of mouse clicks corresponds in resetting the value.
#
# The value min (0) and max (1) are soft bounds because draw() might not be
# called until value is slightly under or over.

def mouseClicked() :
    global value
    global clicks

    clicks += 1 

    if (clicks % 2 == 0) :
        value = 1

def draw() :
    global font
    global value
    global delta

    textFont(font)
    textAlign(LEFT)
    noStroke()

    colorString = "Color: 0x"

    # Stepping down in darkness or stepping up in brightness
    if mouse.pressed :
        if value <= 0 :
            delta = .01
        elif value >= 1 :
            delta = -.01

        value += delta

    # If the value reaches a certain level of darkness or brightness, change
    # the text color for readability
    if value <= .35 :
        textColor = 255     # 255, 255, 255 is white
    elif value > .35 :
        textColor = 0       #   0,   0,   0 is black

    # If the mouse enters the bottom display space color the space white, else
    # fill the space with the color being hovered over or the last color
    # hovered over before the rainbow bounds were left
    #
    # The color code is converted to RGB and the hex value is displayed since
    # this is a more commonly used representation than HSV
    if (mouse.y > 150) :
        rgb = hsv2rgb(0, 0, value)
    else :
        rgb = hsv2rgb(mouse.x / 720.0, (150 - mouse.y) / 150.0, value)
    
    code = "#{0:02x}{1:02x}{2:02x}".format(rgb[0], rgb[1], rgb[2])
    colorMode(RGB, 255, 255, 255)
    fill(rgb[0], rgb[1], rgb[2])
    rect(0, 150, 720, 25)

    colorString += code
    fill(textColor)
    text(colorString, 610, 167)

run()
